import React, {useCallback, useState} from 'react';
import './Navigation.scss';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faMoon, faSun} from '@fortawesome/free-solid-svg-icons';
import {useSelector, useDispatch} from 'react-redux';
import {changeTheme} from '../../store/actions/theme';
import {NavLink} from 'react-router-dom';

import Button from '../UI/Button/Button';
import Auth from '../Auth/Auth';

function Navigation() {
  const links = [
    {to: '/', label: 'Главная'},
    {to: '/team', label: 'Команда'},
    {to: '/materials', label: 'Материалы'},
    {to: '/admin', label: 'Админка'},
  ];

  const isDarkTheme = useSelector(state => state.theme.darkTheme);
  const dispatch = useDispatch();
  const themeChange = useCallback(() => {
    dispatch(changeTheme());
  }, [dispatch]);

  const [showAuth, setShowAuth] = useState(false);

  const togglePopup = () => {
    setShowAuth(!showAuth);
  };

  const renderLinks = () => {
    return links.map((link, idx) => {
      return (
        <NavLink
          key={idx}
          to={link.to}
          className="navigation__link"
          activeClassName="selected"
          exact
        >
          {link.label}
        </NavLink>
      )
    })
  };

  return (
    <div className="container d-flex justify-content-between align-items-center navigation">
      <div className="d-flex navigation__wrapper">
        { renderLinks() }
      </div>

      <div className="d-flex auth__wrapper align-items-center">

        { showAuth ? <Auth closeHandler={togglePopup}/> : null }

        <Button onClick={togglePopup} className="navigation__btn">Авторизация</Button>
        <Button className="navigation__btn">Регистрация</Button>

        <FontAwesomeIcon onClick={themeChange} className="navigation__switcher-theme" icon={isDarkTheme ? faSun : faMoon} size="sm"/>
      </div>
    </div>
  );
}

export default Navigation;
