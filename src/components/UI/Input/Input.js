import React from 'react';
import './Input.scss';

function Input(props) {
  return (
    <input className="input" value={props.value} placeholder={props.placeholder}/>
  );
}

export default Input;
