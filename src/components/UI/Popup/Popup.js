import React from 'react';
import './Popup.scss';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faTimes} from '@fortawesome/free-solid-svg-icons';

import logo from '../../../assets/logo.png';

function Popup(props) {
  return (
    <div className="popup__wrapper">
      <div className="popup__content">
        <FontAwesomeIcon onClick={props.closeHandler} className="popup__close-icon" icon={faTimes} size="lg"/>
        {
          props.withLogo ?
          <div className="popup__content-logo">
            <img src={logo} alt="Corgi Software" width="100"/>
          </div> : null
        }

        <div className="popup__body">
          {props.children}
        </div>
      </div>
    </div>
  );
}

export default Popup;
