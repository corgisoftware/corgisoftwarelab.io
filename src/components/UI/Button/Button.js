import React from 'react';
import './Button.scss';

function Button(props) {
  const classes = ['button'];

  if (props.className) {
    classes.push(props.className);
  }

  return (
    <button onClick={props.onClick} className={classes.join(' ')} style={{width: props.width, height: props.height}}>
      {props.children}
    </button>
  );
}

export default Button;
