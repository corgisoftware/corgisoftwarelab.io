import React from 'react';
import './Auth.scss';
import Popup from '../UI/Popup/Popup';
import Input from '../UI/Input/Input';
import Button from '../UI/Button/Button';

function Auth(props) {
  return (
    <Popup withLogo={true} closeHandler={props.closeHandler}>
      <div className="auth">
        <div className="auth__header">
          Авторизация
        </div>
        <div className="auth__body">

          <div className="auth__body-field">
            <div className="auth__body-field-title">
              Email
            </div>
            <Input placeholder="user@example.com" />
          </div>

          <div className="auth__body-field">
            <div className="auth__body-field-title">
              Пароль
            </div>
            <Input placeholder="123456" />
          </div>

          <div className="auth__body-accept d-flex justify-content-center">
            <Button height="42px" width="180px">Вход</Button>
          </div>

        </div>
      </div>
    </Popup>
  );
}

export default Auth;
