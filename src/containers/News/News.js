import React from 'react';
import './News.css';


function News() {
  return (
     <div>
   <div className="nav-bottom">
        <button type="button" className="btn btn-primary btn-lg">Добавить новость</button>
        <button type="button" className="btn btn-primary btn-lg">Добавить материал</button>
        <button type="button" className="btn btn-primary btn-lg">Добавить карточку</button>
    </div>

    <div className="main-content">
  <div className="content-info d-flex jystify-content-space-between">
      <div className="content-left">Lorem ipsum dolor sit Lorem ipsum dolor
          Lorem ipsum dolor sit Lorem ipsum dolor sit Lorem ipsum dolor sit sit Lorem ipsum dolor sit</div>
      <div className="content-righ">
          <button type="button" className="btn btn-secondary btn-sm">Изменить</button>
          <button type="button" className="btn btn-secondary btn-sm">Удалить</button>
      </div>
  </div>
</div>
<div className="main-content">
  <div className="content-info d-flex jystify-content-space-between">
      <div className="content-left">Lorem ipsum dolor sit Lorem ipsipsum dolor siipsum dolor sivipsum dolor siipsum dolor siipsum dolor siipsum dolor siipsum dolor siipsum dolor sium dolor
          Lorem ipsum dolor sit Lorem ipsum dolor sit Lorem ipsum dolor sit sit Lorem ipsum dolor sit</div>
      <div className="content-righ">
          <button type="button" className="btn btn-secondary btn-sm">Изменить</button>
          <button type="button" className="btn btn-secondary btn-sm">Удалить</button>
      </div>
  </div>
</div>
<div className="main-content">
  <div className="content-info d-flex jystify-content-space-between">
      <div className="content-left">Lorem ipsum dolor sit Lorem ipsuipsum dolor siipsum dolor siipsum dolor siipsum dolor sim dolor
          Lorem ipsum dolor sit Lorem ipsum dolor sit Lorem ipsum dolor sit sit Lorem ipsum dolor sit</div>
      <div className="content-righ">
          <button type="button" className="btn btn-secondary btn-sm">Изменить</button>
          <button type="button" className="btn btn-secondary btn-sm">Удалить</button>
      </div>
  </div>
</div>
<div className="main-content">
  <div className="content-info d-flex jystify-content-space-between">
      <div className="content-left">Lorem ipsum dolipsum dolor siipsum dolor siipsum dolor siipsum dolor siipsum dolor siipsum dolor sior sit Lorem ipsum dolor
          Lorem ipsum dolor sit Lorem ipsum dolor sit Lorem ipsum dolor sit sit Lorem ipsum dolor sit</div>
      <div className="content-righ">
          <button type="button" className="btn btn-secondary btn-sm">Изменить</button>
          <button type="button" className="btn btn-secondary btn-sm">Удалить</button>
      </div>
  </div>
</div>

  </div>



  );
}



export default News;
