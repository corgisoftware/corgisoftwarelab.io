import React from 'react';
import './Header.scss';
import logoLight from '../../assets/corgi-light.png';
import logoDark from '../../assets/corgi-dark.png';
import Navigation from '../../components/Navigation/Navigation';
import {useSelector} from 'react-redux';

function Header() {
  const isDarkTheme = useSelector(state => state.theme.darkTheme);

  return (
    <div className="header__container">
      <div className="header__content__container-logo d-flex align-items-center">
        <div className="container d-flex justify-content-center">
          <img src={isDarkTheme ? logoDark : logoLight} alt="Corgi Software" width="400"/>
        </div>
      </div>
      <div className="header__content__container-navbar d-flex align-items-center">
        <Navigation />
      </div>
    </div>
  );
}

export default Header;
