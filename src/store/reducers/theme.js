import {CHANGE_THEME} from '../actions/actionTypes';

const initialState = {
  darkTheme: false
};

export default function themeReducer(state = initialState, action) {
  switch (action.type) {
    case CHANGE_THEME:
      return {
        darkTheme: !state.darkTheme
      };
    default:
      return state;
  }
}
