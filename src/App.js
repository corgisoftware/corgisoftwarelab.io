import React from 'react';
import './App.scss';
import {Route, Switch, Redirect} from 'react-router-dom';
import {useSelector} from 'react-redux';

import Header from './containers/Header/Header';
import News from './containers/News/News';
import Admin from './containers/Admin/Admin';
import Team from './containers/Team/Team';
import Materials from './containers/Materials/Materials';

function App() {
  const isDarkTheme = useSelector(state => state.theme.darkTheme);
  const classes = ['app-container'];

  if (isDarkTheme) {
    classes.push('dark');
  }

  return (
    <div className={classes.join(' ')}>
      <Header />
      <Switch>
        <Route path="/team" component={Team}/>
        <Route path="/materials" component={Materials}/>
        <Route path="/admin" component={Admin}/>
        <Route path="/" component={News}/>
        <Redirect to="/"/>
      </Switch>
    </div>
  );
}

export default App;
